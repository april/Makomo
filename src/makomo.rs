use serenity::client::Client;
use serenity::framework::standard::StandardFramework;
use serenity::model::id::UserId;
use crate::config::Config;
use crate::handler::Handler;

// Modules

use crate::modules::generic::GENERIC_GROUP;

pub fn init_client(conf: Config) -> Client {
    let mut client = Client::new(&conf.token, Handler).expect("Error creating client.");
    client.with_framework(framework(conf));

    return client
}

fn framework(conf: Config) -> StandardFramework {
    let fw = StandardFramework::new()
        .configure(|c| c
            .prefix(&conf.prefix)
            .with_whitespace(true)
            .owners(vec![UserId(conf.owner_id)].into_iter().collect()))
        .after(|_ctx, msg, cmd_name, error| {
            match error {
                Ok(()) => println!("Processed command '{}' from author {}", cmd_name, msg.author.name),
                Err(why) => println!("The command '{}' gave the error: {:?}", cmd_name, why)
            }
        })
        .group(&GENERIC_GROUP);

    return fw;
}