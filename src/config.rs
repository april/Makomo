use std::path::Path;
use std::fs::File;
use std::io::Read;
use serde_json::from_str;
use serde::{Deserialize, Serialize};
use serde_json::Result;

#[derive(Deserialize, Serialize)]
pub struct Config {
    pub token: String,
    pub prefix: String,
    pub owner_id: u64
}

impl Config {
    pub fn new<P: AsRef<Path>>(file: P) -> Result<Self> {
        let mut file = File::open(file).unwrap();
        let mut data = String::new();
        file.read_to_string(&mut data);
        Ok(from_str(&data)?)
    }
}