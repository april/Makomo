use serenity::prelude::*;
use serenity::model::{
    gateway:: {
        Ready,
        Activity
    }
};

pub struct Handler;

impl EventHandler for Handler {
    fn ready(&self, ctx: Context, ready: Ready) {
        println!("Bot is ready on {}", ready.user.tag());
        println!("Servers: {}", ready.guilds.len());

        let act = Activity::playing("with rust");

        ctx.set_activity(act);
    }
}