#![allow(unused_must_use)]

mod config;
mod makomo;
mod handler;
mod modules;

use config::Config;
use makomo::init_client;

fn main() {
    let conf: Config = Config::new("conf.json").unwrap();

    let mut client = init_client(conf);

    if let Err(why) = client.start() {
        println!("Error starting client. {}", why)
    }
}
